﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Text;

/*
* Dibuat Oleh Danang Sugiarto
* D4 Teknologi Game
* Politeknik Elektronika Negeri Surabaya
*/

public class Client
{
    static Socket acc;
    static TcpClient socketForServer;
    static Thread rec;
    static string name,ip;

    static public void Main(string[] Args)
    {
        Console.WriteLine("Enter Your Name :");
        name = Console.ReadLine();
        Console.WriteLine("Enter Ip Server :");
        ip = Console.ReadLine();
        try
        {
            socketForServer = new TcpClient(ip, 8000);
        }
        catch
        {
            Console.WriteLine(
            "Failed to connect to server at {0}:999", "localhost");
            return;
        }
        Thread th = new Thread(new ThreadStart(TransferData));
        th.Start();
    }

    static void TransferData()
    {
        acc = socketForServer.Client;
        rec = new Thread(new ThreadStart(ReceiveData));
        rec.Start();

        NetworkStream networkStream = socketForServer.GetStream();
        System.IO.StreamReader streamReader =
        new System.IO.StreamReader(networkStream);
        System.IO.StreamWriter streamWriter =
        new System.IO.StreamWriter(networkStream);


        Console.WriteLine("type:");
        string str = Console.ReadLine();
        while (str != "exit")
        {
            streamWriter.WriteLine("[" + name + "] " + str);
            streamWriter.Flush();
            Console.WriteLine("type:");
            str = Console.ReadLine();
        }
        if (str == "exit")
        {
            streamWriter.WriteLine(str);
            streamWriter.Flush();

        }
    }

    static void ReceiveData()
    {
        while (true)
        {
            //Thread.Sleep(500);
            /* Menyiapkan buffer untuk terima data sejumlah 255 byte */
            byte[] buffer = new byte[255];
            /* Menerima data dan menghitung panjangnya */
            int rec = acc.Receive(buffer, 0, buffer.Length, 0);
            /* Memangkas data sesuai dengan panjang data agar pembacaan lebih cepat */
            Array.Resize(ref buffer, rec);
            /* Convert dan Print data */
            Console.WriteLine(Encoding.Default.GetString(buffer));
        }
    }
}