﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections.Generic;

/*
* Dibuat Oleh Danang Sugiarto
* D4 Teknologi Game
* Politeknik Elektronika Negeri Surabaya
*/

public class AsynchIOServer
{
    static TcpListener tcpListener = new TcpListener(8000);
    static List<Socket> clientList;

    static void SendBroadcast(string data)
    {
        foreach(Socket s in clientList)
        {
            byte[] bt = Encoding.Default.GetBytes(data);
            s.Send(bt, 0, bt.Length, 0);
        }
    }

    static void ServerChat()
    {
        while (true)
        {
            byte[] bt = Encoding.Default.GetBytes("[Server] " + Console.ReadLine());
            foreach (Socket s in clientList)
            {
                s.Send(bt, 0, bt.Length, 0);
            }
        }
    }

    static void Listeners()
    {

        Socket socketForClient = tcpListener.AcceptSocket();
        clientList.Add(socketForClient);
        if (socketForClient.Connected)
        {
            //Console.WriteLine("Client:"+socketForClient.RemoteEndPoint+" now connected to server.");
            NetworkStream networkStream = new NetworkStream(socketForClient);
            System.IO.StreamWriter streamWriter =
            new System.IO.StreamWriter(networkStream);
            System.IO.StreamReader streamReader =
            new System.IO.StreamReader(networkStream);

            while (true)
            {
                string theString = streamReader.ReadLine();
                Console.WriteLine("Message recieved by client:" + theString);
                SendBroadcast(theString);
                if (theString == "exit")
                    break;
            }
            streamReader.Close();
            networkStream.Close();
            streamWriter.Close();

        }
        socketForClient.Close();
        Console.WriteLine("Press any key to exit from server program");
        Console.ReadKey();
    }
   
    public static void Main()
    {
        tcpListener.Start();
        clientList = new List<Socket>();
        Console.WriteLine("************| S e r v er |************");
        Console.WriteLine("Hoe many clients are going to connect ?");
        int numberOfClientsYouNeedToConnect =int.Parse( Console.ReadLine());
        for (int i = 0; i < numberOfClientsYouNeedToConnect; i++)
        {
            Thread newThread = new Thread(new ThreadStart(Listeners));
            newThread.Start();
        }
        Thread th = new Thread(new ThreadStart(ServerChat));
        th.Start();
    }
}
