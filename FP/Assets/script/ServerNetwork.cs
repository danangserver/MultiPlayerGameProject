﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;

public class ServerNetwork : MonoBehaviour {

    private static TcpListener tcpListener;
    private static List<Socket> tcpClientsList = new List<Socket>();
    private static List<Data> dataEnemyList = new List<Data>();
    static int number = 1;
    public bool On = false;

    public Data enemy1,enemy2,enemy3, player1, player2, temp;
    public Text textMsg;

    // Use this for initialization
    
    void Start () {
        enemy1 = new Data(1, 20, 30);
        enemy2 = new Data(2, 40, 50);
        enemy3 = new Data(3, 60, 70);
        dataEnemyList.Add(enemy1);
        dataEnemyList.Add(enemy2);
        dataEnemyList.Add(enemy3);
        tcpListener = new TcpListener(IPAddress.Any, 4444);
        tcpListener.Start();

        Debug.Log("Server started");
        for (int i = 0; i < 2; i++)
        {
            Thread th = new Thread(new ThreadStart(ClientListener));
            th.Start();
        }
    }
	
	// Update is called once per frame
	void Update () {
        //if(On)
        //    StartCoroutine(Send());

        if (tcpClientsList.Count >= 0)
        {
            On = false;
            Thread th = new Thread(new ThreadStart(BroadCastPostAI));
            th.Start();
        }
    }

    void ClientListener()
    {
        //TcpClient tcpClient = (TcpClient)obj;
        Debug.Log("Tcp Listen");
        Socket socketForClient = tcpListener.AcceptSocket();
        tcpClientsList.Add(socketForClient);
        if (socketForClient.Connected)
        {
            textMsg.text = "Client "+ number + " connected";
            number++;
            /*foreach(Data data in dataEnemyList)
            {
                byte[] bit = new byte[100];
                bit = ObjectToByteArray(data);
                socketForClient.Send(bit);
            }*/
            while (socketForClient.Connected)
            {
                byte[] receive = new byte[100];
                int rec = socketForClient.Receive(receive, 0, receive.Length, 0);
                Array.Resize(ref receive, rec);
                temp = (Data)ByteArrayToObject(receive);
                if(temp.id == 4)
                {
                    player1 = temp;
                }else if(temp.id == 5)
                {
                    player2 = temp;
                }
                BroadCastPostAI();
            }
        }
    }

    void BroadCast(string msg, Socket Client)
    {
        foreach (Socket client in tcpClientsList)
        {
            if (client != Client)
            {
                NetworkStream networkStream = new NetworkStream(client);
                StreamWriter streamWriter = new StreamWriter(networkStream);
                streamWriter.WriteLine(msg);
                streamWriter.Flush();
            }
        }
    }

    void BroadCastPostAI()
    {
        foreach (Socket client in tcpClientsList)
        {
            foreach (Data enemy in dataEnemyList)
            {
                byte[] bit = new byte[100];
                bit = ObjectToByteArray(enemy);
                client.Send(bit);
            }
        }
    }

    byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    System.Object ByteArrayToObject(byte[] _packets)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(_packets, 0, _packets.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        System.Object obj = binForm.Deserialize(memStream);
        return obj;
    }

    IEnumerator Send()
    {
        //Mengirim ke Semua Jaringan
        var Server = new UdpClient();
        var RequestData = Encoding.ASCII.GetBytes("You has been joined the game !");
        Server.EnableBroadcast = true;
        Server.Send(RequestData, RequestData.Length, new IPEndPoint(IPAddress.Broadcast, 8888));
        Debug.Log("Waiting for client...");
        yield return new WaitForSeconds(0.01f);
    }
}
