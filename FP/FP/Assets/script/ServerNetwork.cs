﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;

public class ServerNetwork : MonoBehaviour {

    private static TcpListener tcpListener;
    private static List<Socket> tcpClientsList = new List<Socket>();
    static int number = 1;
    public bool On = false;

    public Text textMsg;

    AI enemy1;
    // Use this for initialization
    
    void Start () {
        enemy1 = new AI(1, 2, 3);
        tcpListener = new TcpListener(IPAddress.Any, 4444);
        tcpListener.Start();

        Debug.Log("Server started");
        for (int i = 0; i < 2; i++)
        {
            Thread th = new Thread(new ThreadStart(ClientListener));
            th.Start();
        }
    }
	
	// Update is called once per frame
	void Update () {
        //if(On)
        //    StartCoroutine(Send());

        if (tcpClientsList.Count >= 0)
        {
            On = false;
            BroadCastPostAI(enemy1);
        }
    }

    void ClientListener()
    {
        //TcpClient tcpClient = (TcpClient)obj;
        Debug.Log("Tcp Listen");
        Socket socketForClient = tcpListener.AcceptSocket();
        tcpClientsList.Add(socketForClient);
        if (socketForClient.Connected)
        {
            NetworkStream networkStream = new NetworkStream(socketForClient);
            StreamReader reader = new StreamReader(networkStream);


            textMsg.text = "Client {0} connected" + number;
            number++;
            byte[] bit = new byte[100];
            bit = ObjectToByteArray(enemy1);
            socketForClient.Send(bit,0,bit.Length,0);
            enemy1 = (AI)ByteArrayToObject(bit);
            textMsg.text = enemy1.id.ToString();
            /*while (socketForClient.Connected)
            {
                //string message = reader.ReadLine();
                //Console.WriteLine(message);
                //BroadCast(message, socketForClient);
            }
            reader.Close();
            networkStream.Close();*/
        }
    }

    void BroadCast(string msg, Socket Client)
    {
        foreach (Socket client in tcpClientsList)
        {
            if (client != Client)
            {
                NetworkStream networkStream = new NetworkStream(client);
                StreamWriter streamWriter = new StreamWriter(networkStream);
                streamWriter.WriteLine(msg);
                streamWriter.Flush();
            }
        }
    }

    void BroadCastPostAI(object msg)
    {
        foreach (Socket client in tcpClientsList)
        {
            NetworkStream networkStream = new NetworkStream(client);
            StreamWriter streamWriter = new StreamWriter(networkStream);
            streamWriter.WriteLine(msg);
            streamWriter.Flush();
        }
    }

    byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
            return null;
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    System.Object ByteArrayToObject(byte[] _packets)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(_packets, 0, _packets.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        System.Object obj = binForm.Deserialize(memStream);
        return obj;
    }

    IEnumerator Send()
    {
        //Mengirim ke Semua Jaringan
        var Server = new UdpClient();
        var RequestData = Encoding.ASCII.GetBytes("You has been joined the game !");
        Server.EnableBroadcast = true;
        Server.Send(RequestData, RequestData.Length, new IPEndPoint(IPAddress.Broadcast, 8888));
        Debug.Log("Waiting for client...");
        yield return new WaitForSeconds(0.01f);
    }
}

[Serializable]
public class AI
{
    public float id, postXai, postYai;

    public AI(float _id, float _postXai, float _postYai)
    {
        id = _id;
        postXai = _postXai;
        postYai = _postYai;
    }
}
