﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;


public class NetworkManager : MonoBehaviour
{

    Socket acc;
    TcpClient tcpClient;
    Thread rec;

    Player packet, player, otherplayer;
    Enemy enemy = new Enemy(4,5,6);

    public Text textID, textX, textY, textMsg;
    ///public GameManager manager;

    public static int tempScore = 0;

    IPEndPoint ServerEP;

    UdpClient udpClient;
    
    void Awake()
    {
        JoinTcp();
        textID.text = enemy.id.ToString();
        textX.text = enemy.postXenemy.ToString();
        textY.text = enemy.postYenemy.ToString();
    }

    public void JoinUdp()
    {
        udpClient = new UdpClient(8888);
        ServerEP = new IPEndPoint(IPAddress.Any, 8888);
        byte[] ServerRequestData = udpClient.Receive(ref ServerEP);
        string ServerRequest = Encoding.ASCII.GetString(ServerRequestData);
        textMsg.text = ServerRequest;
        udpClient.Close();
        JoinTcp();
    }
    
    void JoinTcp()
    {       
        try
        {
            //tcpClient = new TcpClient(ServerEP.Address.ToString(), 4444);
            tcpClient = new TcpClient("192.168.8.100", 4444);
            textMsg.text = "Connected to server with tcp.";

            Thread thread = new Thread(ReceiveData);
            thread.Start(tcpClient);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
   
    }

    public void SentScore(int score)
    {
        StartCoroutine(TransferData(score.ToString()));
    }

    IEnumerator TransferData(string data)
    {
        acc = tcpClient.Client;

        NetworkStream networkStream = tcpClient.GetStream();
        System.IO.StreamReader streamReader =
        new System.IO.StreamReader(networkStream);
        System.IO.StreamWriter streamWriter =
        new System.IO.StreamWriter(networkStream);

        if (data != "exit")
        {
            streamWriter.WriteLine(data);
            streamWriter.Flush();
        }
        if (data == "exit")
        {
            streamWriter.WriteLine(data);
            streamWriter.Flush();

        }
        yield return new WaitForSeconds(0.01f);
    }

    void ReceiveData(object s)
    {
        TcpClient tcpClient = (TcpClient)s;
        byte[] buffer = new byte[1000];
        Socket socket = tcpClient.Client;
        while (socket.Connected)
        {
            int rec = socket.Receive(buffer, 0, buffer.Length, 0);
            Array.Resize(ref buffer, rec);
            //tempScore += Convert.ToInt32(Encoding.Default.GetString(buffer));
            Debug.Log("Receive Data");
            enemy = (Enemy)ByteArrayToObject(buffer);
            Debug.Log("INI : " + enemy.id + " | " + enemy.postXenemy + " | " + enemy.postYenemy);
            textID.text = enemy.id.ToString();
            textX.text = enemy.postXenemy.ToString();
            textY.text = enemy.postYenemy.ToString();

        }
    }

    byte[] ObjectToByteArray(object obj)
    {
        if (obj == null)
        {
            return null;
        }
        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }

    System.Object ByteArrayToObject(byte[] _packets)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(_packets, 0, _packets.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        System.Object obj = binForm.Deserialize(memStream);
        Debug.Log("ISI");
        return obj;
    }
}

[Serializable]
public class Player
{
    public float id, postXplayer, postYplayer, Healthplayer;

    public Player(float _id, float _postX, float _postY, float _Health)
    {
        id = _id;
        postXplayer = _postX;
        postYplayer = _postY;
        Healthplayer = _Health;
    }
}

[Serializable]
public class Enemy
{
    public float id, postXenemy, postYenemy;

    public Enemy(float _id, float _postXenemy, float _postYenemy)
    {
        id = _id;
        postXenemy = _postXenemy;
        postYenemy = _postYenemy;
    }
}
