﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Server
{
    class send
    {
        private static TcpListener tcpListener;
        private static List<Socket> tcpClientsList = new List<Socket>();

        static int number = 1;

        static void Main(string[] args)
        {
            Thread WantClient = new Thread(Send);
            WantClient.Start();
            
            tcpListener = new TcpListener(IPAddress.Any, 4444);
            tcpListener.Start();

            Console.WriteLine("Server started");
            for(int i = 0; i < 2; i++)
            {
                Thread th = new Thread(new ThreadStart(ClientListener));
                th.Start();
            }
        }

        public static void ClientListener()
        {
            //TcpClient tcpClient = (TcpClient)obj;
            Console.WriteLine("Tcp Listen");
            Socket socketForClient = tcpListener.AcceptSocket();
            tcpClientsList.Add(socketForClient);
            if (socketForClient.Connected)
            {
                NetworkStream networkStream = new NetworkStream(socketForClient);
                StreamReader reader = new StreamReader(networkStream);


                Console.WriteLine("Client {0} connected", number);
                number++;

                while (socketForClient.Connected)
                {
                    string message = reader.ReadLine();
                    Console.WriteLine(message);
                    BroadCast(message, socketForClient);
                }
                reader.Close();
                networkStream.Close();
            }
        }
        
        public static void BroadCast(string msg, Socket Client)
        {
            foreach (Socket client in tcpClientsList)
            {
                if (client != Client)
                {
                    NetworkStream networkStream = new NetworkStream(client);
                    StreamWriter streamWriter = new StreamWriter(networkStream);
                    streamWriter.WriteLine(msg);
                    streamWriter.Flush();
                }
            }
        }

        static void Send()
        {
            //Mengirim ke Semua Jaringan
            var Server = new UdpClient();
            var RequestData = Encoding.ASCII.GetBytes("You has been joined the game !");

            while (true)
            {
                Server.EnableBroadcast = true;
                Server.Send(RequestData, RequestData.Length, new IPEndPoint(IPAddress.Broadcast, 8888));
            }
        }
    }
}
