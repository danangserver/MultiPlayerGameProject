﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Udp_Client
{
    class Program
    {

        static UdpClient client;

        static void Main(string[] args)
        {
            client = new UdpClient();
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("10.252.39.239"), 11000); // endpoint where server is listening
            client.Connect(ep);
            Thread th = new Thread(ReceiveData);
            th.Start(ep);

            // send data
            while (true)
            {
                string data = Console.ReadLine();
                byte[] dt = new byte[100];
                dt = Encoding.Default.GetBytes(data);
                client.Send(dt, dt.Length);
            }
            // then receive data

            //client.Close();
            /*
            Console.Read();
            Console.Read();
            Console.Read();*/
        }

        static void ReceiveData(object o)
        {
            while (true)
            {
                IPEndPoint ep = (IPEndPoint)o;
                byte[] receivedData = new byte[100];
                receivedData = client.Receive(ref ep);

                Console.WriteLine(Encoding.Default.GetString(receivedData));
            }
        }
    }
}
