﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Udp_Networking
{
    class Program
    {
        static UdpClient udpServer;

        static void Main(string[] args)
        {
            udpServer = new UdpClient(11000);

            while (true)
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 11000);
                byte[] data = new byte[100];
                data = udpServer.Receive(ref remoteEP); // listen on port 11000
                Console.WriteLine("receive data from " + remoteEP.ToString() + " || " + Encoding.Default.GetString(data));
                udpServer.Send(data, data.Length, remoteEP);
            }
        }
    }
}
