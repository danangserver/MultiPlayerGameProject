﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Pose = Thalmic.Myo.Pose;

public class GameManager : MonoBehaviour {

    public enum status
    {
        OnUI,
        Play,
        GameOver
    }

    public status stat;
    public Spawnner spawnScript;
    public GameObject Panel, Image, Slider, Win, Lose, Draw, PanelScore;
    public Text textHS, textES, textDR;

    public bool play = false;
    public bool start = false;

    Pose _lastPose = Pose.Unknown;
    bool gameOver = false;

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.GetFloat("Score") == 0)
            PlayerPrefs.SetFloat("Score", 0);
	}
	
	// Update is called once per frame
	void Update () {
        if (stat == status.OnUI)
        {
            Image.transform.localScale = new Vector2(0, 0);
            Slider.transform.localScale = new Vector2(0, 0);
            Panel.transform.localScale = new Vector2(1, 1);

            if (spawnScript.thalmicMyo.pose != _lastPose)
            {
                _lastPose = spawnScript.thalmicMyo.pose;

                // Vibrate the Myo armband when a fist is made.
                if (spawnScript.thalmicMyo.pose == Pose.Fist)
                {
                    stat = status.Play;
                    spawnScript.ExtendUnlockAndNotifyUserAction(spawnScript.thalmicMyo);
                }
            }
            
            if (Input.GetKeyDown(KeyCode.Space) && start)
            {
                PanelScore.transform.localScale = new Vector2(1, 1);
                stat = status.Play;
            }
        }
        else if (stat == status.Play)
        {
            Image.transform.localScale = new Vector2(1, 1);
            Slider.transform.localScale = new Vector2(1, 1);
            Panel.transform.localScale = new Vector2(0, 0);
            play = true;
        }
        else if (stat == status.GameOver)
        {
            //offline
            /*
            if(PlayerPrefs.GetFloat("Score") > spawnScript.Score)
            {
                PlayerPrefs.SetFloat("Score", spawnScript.Score);
                textHS.text = spawnScript.Score.ToString();
                Highscore.transform.localScale = new Vector2(1, 1);
            }
            else
            {
                YourScore.transform.localScale = new Vector2(1, 1);
                textYS.text = spawnScript.Score.ToString();
            }
            */
            if(spawnScript.Score > Convert.ToInt32(spawnScript.ScoreLawan.text))
            {
                textHS.text = spawnScript.Score.ToString();
                Win.transform.localScale = new Vector2(1, 1);
            }
            else if(spawnScript.Score < Convert.ToInt32(spawnScript.ScoreLawan.text))
            {
                textES.text = spawnScript.Score.ToString();
                Lose.transform.localScale = new Vector2(1, 1);
            }
            else
            {
                textDR.text = spawnScript.Score.ToString();
                Draw.transform.localScale = new Vector2(1, 1);
            }
            StartCoroutine(delay());
        }

        if (spawnScript.Timer < 0)
            gameOver = true;
        if (gameOver)
        {
            stat = status.GameOver;
            gameOver = false;
        }
	}

    public void OnClick()
    {
        stat = status.Play;
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(0);
    }
}
