﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NetworkManager : MonoBehaviour {

    Socket acc;
    TcpClient socketForServer;
    Thread rec;

    public GameManager manager;

    public static int tempScore = 0;

    //Data Parse
    //public

    void Awake()
    {

    }

    // Use this for initialization
    void Start () {
        try
        {
            socketForServer = new TcpClient("192.168.8.100", 8000);
            if (socketForServer.Connected)
            Debug.Log("Connected");
            //rec = new Thread(new ThreadStart(ReceiveData));
            //rec.Start();
            rec = new Thread(ReceiveData);
            rec.Start(socketForServer);
        }
        catch
        {
            Debug.Log(
            "Failed to connect to server at {0}:999" + "localhost");
            return;
        }
    }
	
	// Update is called once per fram
	void Update () {
        //rec = new Thread(new ThreadStart(ReceiveData));
        //rec.Start();
    }

    public void SentScore(int score)
    {
        //StartCoroutine(TransferData(score.ToString()));
        StartCoroutine(TransferData(score.ToString()));
    }

    IEnumerator TransferData(string data)
    {
        acc = socketForServer.Client;
        
        NetworkStream networkStream = socketForServer.GetStream();
        System.IO.StreamReader streamReader =
        new System.IO.StreamReader(networkStream);
        System.IO.StreamWriter streamWriter =
        new System.IO.StreamWriter(networkStream);

        if (data != "exit")
        {
            streamWriter.WriteLine(data);
            streamWriter.Flush();
        }
        if (data == "exit")
        {
            streamWriter.WriteLine(data);
            streamWriter.Flush();

        }
        yield return new WaitForSeconds(0.01f);
    }

    /*
    static void ReceiveData(object s)
    {
        while (true)
        {
            //Debug.Log("Running");
            TcpClient tcpCLient = (TcpClient)s;

            System.IO.StreamReader streamReader =
            new System.IO.StreamReader(tcpCLient.GetStream());

            string str = streamReader.ReadLine();
            Debug.Log(str);
            //Thread.Sleep(500);
            /* Menyiapkan buffer untuk terima data sejumlah 255 byte 
            //byte[] buffer = new byte[255];
            /* Menerima data dan menghitung panjangnya 
            //int rec = acc.Receive(buffer, 0, buffer.Length, 0);
            /* Memangkas data sesuai dengan panjang data agar pembacaan lebih cepat 
            //Array.Resize(ref buffer, rec);
            //Debug.Log("|" + Encoding.Default.GetString(buffer));
            tempScore = Convert.ToInt32(str);
            /* Convert dan Print data 
            //Console.WriteLine(Encoding.Default.GetString(buffer));
        }
    }*/

    void ReceiveData(object s)
    {
        TcpClient tcpClient = (TcpClient)s;
        byte[] buffer = new byte[1000];
        Socket socket = tcpClient.Client;
        while (socket.Connected)
        {
            int rec = socket.Receive(buffer, 0, buffer.Length, 0);
            Array.Resize(ref buffer, rec);
            if (Encoding.Default.GetString(buffer) == "start")
                manager.start = true;
            else
                tempScore += Convert.ToInt32(Encoding.Default.GetString(buffer));
            Debug.Log(tempScore);
        }
    }
}
