﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;

public class Spawnner : MonoBehaviour {

    public enum QuestStatus
    {
        Available,
        OnProgress,
        OnAction,
        Matching,
        Complete,
        Destroy
    }

    public QuestStatus m_Status;
    private Pose _lastPose = Pose.Unknown;

    public GameManager manager;
    public NetworkManager network;

    public GameObject myo = null;

    public GameObject[] ObjectQuest;
    public Text textScore, ScoreLawan;
    public Slider slider;
    public ThalmicMyo thalmicMyo;

    public int ValueQuest = 1;
    public float Speed = 2;
    public int Answer = 0;
    public int Validate = 0;
    public int Score = 0;
    public float Timer = 30;
    public string satu, dua;
    bool Answered = false;
	// Use this for initialization
	void Start () {
        thalmicMyo = GameObject.Find("Hub - 1 Myo").GetComponent<ThalmicMyo>();
        slider.maxValue = Timer;
	}
	
	// Update is called once per frame
	void Update () {
        thalmicMyo = myo.GetComponent<ThalmicMyo>();
        ScoreLawan.text = NetworkManager.tempScore.ToString();
        if (manager.play)
        {
            Timer -= Time.deltaTime;
            slider.value = Timer;
            if (m_Status == QuestStatus.Available)
            {
                ValueQuest = Random.Range(1, 5);
                ObjectQuest[ValueQuest - 1].transform.position = new Vector2(0, 8);
                m_Status = QuestStatus.OnProgress;
            }

            if (m_Status == QuestStatus.OnProgress)
            {
                if (ObjectQuest[ValueQuest - 1].transform.position.y > 0)
                {
                    ObjectQuest[ValueQuest - 1].transform.Translate(Vector2.down * Speed);
                }
                else
                {
                    m_Status = QuestStatus.OnAction;
                }
            }

            if (m_Status == QuestStatus.OnAction)
            {
                //input Dance Pad
                if (Input.GetButtonDown(satu))
                {
                    Answer = 1;
                    Answered = true;
                }
                else if (Input.GetButtonDown(dua))
                {
                    Answer = 2;
                    Answered = true;
                }

                //input keyboard
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    Answer = 1;
                    Answered = true;
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    Answer = 2;
                    Answered = true;
                }

                //input Myo
                if (thalmicMyo.pose != _lastPose)
                {
                    _lastPose = thalmicMyo.pose;

                    // Vibrate the Myo armband when a fist is made.
                    if (thalmicMyo.pose == Pose.Fist)
                    {
                        thalmicMyo.Vibrate(VibrationType.Medium);

                        ExtendUnlockAndNotifyUserAction(thalmicMyo);

                        // Change material when wave in, wave out or double tap poses are made.
                    }
                    else if (thalmicMyo.pose == Pose.WaveIn)
                    {
                        //renderer.material = waveInMaterial;
                        Answer = 2;
                        Answered = true;
                        ExtendUnlockAndNotifyUserAction(thalmicMyo);
                    }
                    else if (thalmicMyo.pose == Pose.WaveOut)
                    {
                        //renderer.material = waveOutMaterial;
                        Answer = 1;
                        Answered = true;
                        ExtendUnlockAndNotifyUserAction(thalmicMyo);
                    }
                    else if (thalmicMyo.pose == Pose.DoubleTap)
                    {
                        //renderer.material = doubleTapMaterial;

                        ExtendUnlockAndNotifyUserAction(thalmicMyo);
                    }
                }

                if (Answered)
                {
                    m_Status = QuestStatus.Matching;
                    Answered = false;
                }
            }

            if (m_Status == QuestStatus.Matching)
            {
                if (ValueQuest == 1 && Answer == 1)
                {
                    Validate = 1;
                }
                else if (ValueQuest == 2 && Answer == 2)
                {
                    Validate = 1;
                }
                else if (ValueQuest == 3 && Answer == 2)
                {
                    Validate = 1;
                }
                else if (ValueQuest == 4 && Answer == 1)
                {
                    Validate = 1;
                }
                else
                {
                    Validate = 0;
                }
                m_Status = QuestStatus.Complete;
            }

            if (m_Status == QuestStatus.Complete)
            {
                if (Validate == 1)
                {
                    Score += 1;
                    Timer += 0.5f;
                    //network.SentScore(Score);
                    network.SentScore(1);
                }
                else if (Validate == 0)
                {
                    if(Score >= 0)
                        //Score -= 1;
                    Timer -= 0.5f;
                    //network.SentScore(Score);
                    //network.SentScore(-1);
                }
                textScore.text = Score.ToString();
                m_Status = QuestStatus.Destroy;
            }

            if (m_Status == QuestStatus.Destroy)
            {
                ObjectQuest[ValueQuest - 1].transform.position = new Vector2(100, 100);
                m_Status = QuestStatus.Available;
            }
        }
    }

    public void ExtendUnlockAndNotifyUserAction(ThalmicMyo myo)
    {
        ThalmicHub hub = ThalmicHub.instance;

        if (hub.lockingPolicy == LockingPolicy.Standard)
        {
            myo.Unlock(UnlockType.Timed);
        }

        myo.NotifyUserAction();
    }
}
